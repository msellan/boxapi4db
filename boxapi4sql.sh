#!/bin/bash

#------------------------------------------------------------------------------------
#   Script:  boxapi4db.sh
#
#   Purpose: This script utilizes the Box.com API to allow interaction with 
#	     Box.com to view, read, write, and change data in Box folders/files.
#	     Box uses the OAuth2 four-legged authentiation mechanism which consists
#	     of token exchanges.
#	    
#	     To set this up one must start with a valid Box.com account and create
#	     a Box application with a Box developer account.  Then you have to 
#	     manually authorize the application the first time (see get_initial_tokens
#	     below).  
#
#	     To store the tokens, token metadata, and Box credentials, this script
#	     uses a local MySQL database.  That should be initialized first with
#	     a database user that is set up using the mysql_config_editor which
#	     encrypts and stores your database user into a file on the filesystem
#	     at /home/shellusername/.mylogin.cnf.  Once that's set up you can 
# 	     call the mysql client with the --login-path=local rather than 
#	     login credentials with each datbase request.
#
#  Author:  Mark Sellan
#
#  Date:    December 29th, 2016
#
#-----------------------------------------------------------------------------------
#  Modifications:
#-----------------------------------------------------------------------------------

#set -x


SCRIPT=`basename $0`
LOG_DIR=/var/log/boxapi4db/log
PATH=${PATH}:/usr/bin/:/usr/local/bin:/Applications/MAMP/Library/bin

#set -x 

#------->   get_initial_tokens   <-------------------------------------------------
#
#    This is a manual "one-time" process to enable the applicaiton with Box.com
#    You must maunally login to the Box interface to generate and retrieve a"code"
#    that is returned to you in the browser.  Then, that code is plugged into a POST
#    request via a tool like "Postman" to initiate an OAuth2 session to generate the
#    first set of refresh and access tokens.  
#----------------------------------------------------------------------------------

generate_URL () {

my_client_id=$(get_client_id)
echo "Enter the following URL into a browser window: https://account.box.com/api/oauth2/authorize?response_type=code&client_id=${my_client_id}&state=authenticated"

}

#------->   get_new_tokens   <-------------------------------------------------------
#
#    This is a function used to exchange tokens.  Once the OAuth2 process
#    has been initiated it can be sustained by requesting a new set of tokens,
#    specifically using the api call 'refresh_token' at least once every 60 days.
#    
#    The access_token is used to interact with Box and is valid for 1 hour. 
#    After one hour this routine will need to be called again to get a new set of 
#    access and refresh tokens assigned.
#------------------------------------------------------------------------------------

get_new_tokens () {

valid_refresh_token=$(retrieve_refresh_token)
my_client_id=$(get_client_id)
my_client_secret=$(get_client_secret)

token_responses=`curl "https://api.box.com/oauth2/token" -d "grant_type=refresh_token&refresh_token=${valid_refresh_token}&client_id=${my_client_id}&client_secret=${my_client_secret}" -X POST`

if [[ `echo ${token_responsea} | grep -o error` ]]
then
	echo "There's an error: do not write token file"
else

	echo "${token_responses}" > /Users/msellan/Documents/current_tokens.dat
	file_store_tokens
fi

}

#------->   write_row  <----------------------------------------------------------
#
#   This function writes a complete updated row to the database. All columns are
#   updated including access_token, expires_in, restricted_to, refresh_token, 
#   token_type, token_time, client_id and client_secret.
#--------------------------------------------------------------------------------- 

write_row () {

IFS=,

read access_token expires_in restricted_to refresh_token token_type token_date token_time < test.csv

export access_token expires_in restricted_to refresh_token token_type token_date token_time 

export sql_statement1="USE boxapidata; INSERT INTO keydata (access_token,expires_in,restricted_to,refresh_token,token_type,token_create_date,token_create_time) VALUES ("`echo "'${access_token}','${expires_in}','${restricted_to}','${refresh_token}','${token_type}','${token_date}','${token_time}');"`

echo "${sql_statement1}" | mysql --login-path=local

}

#------->    read_row      <--_-----------------------------------------------------
#
#   This function reads the most recent full row from the database.  All columns are
#   read including access_token, expires_in, restricted_to, refresh_token, token_type
#   token_time, client_id, and client_secret.  To obtain the last row the sql state-
#   ment filters by ordering descending with a limit of 1
#-----------------------------------------------------------------------------------

read_row () {

mydata=`mysql --login-path=local boxapidata -s -N -e "SELECT * from keydata ORDER BY ID DESC LIMIT 1"`
echo $mydata

}

#------->   store_tokens   <--------------------------------------------------------
#    
#    This function parses the output of the get_new_tokens request process getting
#    the key/value pairs and then scrubs out the extraneous brackets and quotes
#    leaving a file storing the current access and refresh tokens.  This file is
#    security restrictred. 
#-----------------------------------------------------------------------------------

db_store_tokens () {

current_token_data=`read_sql`
echo $current_token_data

}

file_store_tokens () {

while IFS=',' read -ra ADDR; do
        for i in "${ADDR[@]}"; do
        echo "$i"
done
done <<< `cat /Users/msellan/Documents/current_tokens.dat` | sed 's/[{}"]//g' > /Users/msellan/Documents/scrubbed_tokens.txt
}

# ------->  retrieve_refresh_token  <-----------------------------------------------
#    
#   This function retrieves a refresh token from the MySQL database by reading the
#   most recently written row.
#-----------------------------------------------------------------------------------

retrieve_refresh_token () {

refresh_token=`mysql --login-path=local boxapidata -s -N -e "SELECT refresh_token from keydata ORDER BY ID DESC LIMIT 1"`
echo ${refresh_token}

}

# ------->  retrieve_access_token  <-----------------------------------------------
#    
#   This function retrieves an access token from the MySQL database by reading the
#   most recently written row.
#-----------------------------------------------------------------------------------

retrieve_access_token () {

access_token=`mysql --login-path=local boxapidata -s -N -e "SELECT access_token from keydata ORDER BY ID DESC LIMIT 1"`
echo ${access_token}

}

get_client_id () {

my_client_id=`mysql --login-path=local boxapidata -s -N -e "SELECT client_id from keydata ORDER BY ID DESC LIMIT 1"`
echo $my_client_id
}

get_client_secret () {

my_client_secret=`mysql --login-path=local boxapidata -s -N -e "SELECT client_secret from keydata ORDER BY ID DESC LIMIT 1"`
echo $my_client_secret

}

get_current_user () {

valid_access_token=$(retrieve_access_token)
current_user=`curl -w %{http_code} "https://api.box.com/2.0/users/me" -H "Authorization: Bearer ${valid_access_token}"`
echo ${current_user}

}


get_folder_info () {

valid_access_token=$(retrieve_access_token)
folder_info=`curl "https://api.box.com/2.0/folders/0" -H "Authorization: Bearer ${valid_access_token}" -v`
echo ${folder_info}

}

get_folder_items () {

valid_access_token=$(retrieve_access_token)
folder_items=`curl "https://api.box.com/2.0/folders/8346181629/items?limit=2&offset=0" -H "Authorization: Bearer ${valid_access_token}" -w %{http_code}`
echo ${folder_items}

}

get_usage () { 


echo "usage:  ${SCRIPT} [-rrt] [-rat] [-gnt] [-gcu] [-gfd] [-gfi] [-gci] [-gul] [-rr] [-wr]"
echo "	"
echo "	where	-rrt retrieves current refresh token from the database"
echo "		-rat retrieves current access token from the database"
echo "		-gnt gets a new set of access and refresh tokens from Box.com"
echo "		-gcu gets details about the current Box user"
echo "		-gfd gets a listing of items in a folder"
echo "		-rrt retrieves current refresh token from the database"
echo "		-rrt retrieves current refresh token from the database"
echo "		-rrt retrieves current refresh token from the database"
echo "		-rrt retrieves current refresh token from the database"
echo "		-rrt retrieves current refresh token from the database"
echo " "
echo "You must select one option.  Selecting zero or more than one results in the dislay of "
echo "this usage statement."

}

# Main

[[ $# -eq 0 ]] && get_usage && exit -1

while [[ $# -ne 0 ]]; do
	case "$1" in
	
		    -rrt) retrieve_refresh_token
		    ;;
		    -rat) retrieve_access_token
		    ;;
		    -gnt) get_new_tokens
   		    ;;
		    -gcu) get_current_user
		    ;;
		    -gfd) get_folder_info
		    ;;
		    -gfi) get_folder_items
		    ;;
		    -gci) get_client_id
		    ;;
		    -gul) get_URL	
		    ;;
		    -rr) read_row
		    ;;
		    -wr) write_row
		    ;;
	esac
    shift
  done


#get_new_tokens
#retrieve_refresh_token
#retrieve_access_token
#get_current_user
#get_folder_info
#get_folders_items
#store_tokens
#write_row
#read_sql
#db_store_tokens
#get_client_id
#get_client_secret
#generate_URL
