# boxapi4db
A bash script to authenticate to Box.com using OAuth2

This script automates the authentication process to use resources on Box.com via a Bash script and a MySQL database for storing client
secrets and access/refresh tokens from Box.

This is a work in progress.  Use at your own risk.
